package init_shim

import (
	"fmt"
	"github.com/klauspost/compress/gzip"
	"github.com/sassoftware/go-rpmutils/cpio"
	"golang.org/x/sys/unix"
	"gopkg.in/freddierice/go-losetup.v1"
	"io"
	"os"
	"path/filepath"
)

const (
	MountPoint    string = "/mnt"
	Binary        string = "/init"
	TargetDir     string = "/"
	DeviceGlob    string = "/dev/sd*"
	InitramfsGlob string = "/initramfs*"
	DeviceLabel   string = "kupfer_boot"
)

func Init() {
	fmt.Println("Init: start")
	files, _ := filepath.Glob(DeviceGlob)
	fmt.Println(files)
	var info *blkInfo
	var err error
	found := false
	for _, file := range files {
		info, err = checkLabel(file)
		if err != nil {
			panic(err.Error())
		}
		if info != nil {
			found = true
			break
		}
	}
	if found {
		fmt.Println("path:", info.path, "label:", info.label, "format", info.format)
	} else {
		panic("no label found")
	}
	err = unix.Mount(info.path, MountPoint, info.format, uintptr(0), "")
	if err != nil {
		panic(err.Error())
	}
	defer unix.Unmount(MountPoint, 0)
	_, err = os.Stat(TargetDir)
	if err != nil && !os.IsNotExist(err) {
		panic(err)
	}
	if os.IsNotExist(err) {
		err = os.Mkdir(TargetDir, 0750)
		if err != nil {
			panic(err)
		}
	}
	var fd *os.File
	var initFiles []string
	initFiles, err = filepath.Glob(MountPoint + InitramfsGlob)
	fd, err = os.Open(initFiles[0])
	if err != nil {
		panic(err)
	}
	if fd == nil {
		panic("fd was nil")
	}
	fmt.Println("file was opened")
	var decompressedInit io.Reader
	decompressedInit, err = gzip.NewReader(fd)
	if err != nil {
		panic(err)
	}
	if decompressedInit == nil {
		panic("decompressedInit was nil")
	}
	fmt.Println("decompressed opened")
	fmt.Println("extracting to:", TargetDir)
	err = cpio.Extract(decompressedInit, TargetDir)
	if err != nil {
		panic(err)
	}
	//reset fd
	_, err = fd.Seek(0, io.SeekStart)
	if err != nil {
		panic(err)
	}
	decompressedInit, err = gzip.NewReader(fd)
	if err != nil {
		panic(err)
	}
	cpioStream := cpio.NewCpioStream(decompressedInit)
	var cpioEntry *cpio.CpioEntry
	cpioEntry, err = cpioStream.ReadNextEntry()
	if err != nil {
		panic(err)
	}
	for cpioEntry.Header.Filename() != cpio.TRAILER {
		func() {
			defer func() {
				cpioEntry, err = cpioStream.ReadNextEntry()
				if err != nil {
					panic(err)
				}
			}()
			fileName := TargetDir + "/" + cpioEntry.Header.Filename()
			mode := cpioEntry.Header.Mode()
			if (mode&cpio.S_ISDIR == 0) && ((mode&cpio.S_ISLNK) == cpio.S_ISLNK || (mode&cpio.S_ISSOCK) == cpio.S_ISSOCK) {
				return
			}
			mode &= 0777
			fmt.Printf("fixing permissions on: %s mode: %#o\n", fileName, mode)
			err = unix.Chmod(fileName, uint32(mode))
			if err != nil {
				panic(err.Error() + "" + fileName)
			}
			err = unix.Chown(fileName, cpioEntry.Header.Uid(), cpioEntry.Header.Gid())
			if err != nil {
				panic(err)
			}
		}()
	}
	err = unix.Exec(TargetDir+Binary, os.Args, os.Environ())
	if err != nil {
		panic(err)
	}
}

func checkLabel(path string) (info *blkInfo, err error) {
	info, err = readBlkInfo(path)
	fmt.Println("checking:", path, "label is '", info.label, "'")
	if err != nil {
		return nil, err
	}
	if info.label == DeviceLabel {
		return info, nil
	}
	switch info.format {
	case "gpt":
		var dev losetup.Device
		for _, gptPart := range info.data.Partitions() {
			dev, err = losetup.Attach(info.path, gptPart.startOffset, false)
			if err != nil {
				fmt.Println(err.Error())
				fmt.Println(dev.Detach())
				continue
			}
			info, err = checkLabel(dev.Path())
			if err != nil {
				fmt.Println(dev.Detach())
				fmt.Println(err.Error())
				continue
			}
			if info != nil {
				return info, nil
			}
			fmt.Println(dev.Detach())
		}
	}
	return nil, nil
}
