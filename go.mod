module init-shim

go 1.18

require (
	github.com/cavaliergopher/cpio v1.0.1
	github.com/google/go-tpm v0.3.3
	github.com/google/renameio v1.0.1
	github.com/insomniacslk/dhcp v0.0.0-20220504074936-1ca156eafb9f
	github.com/jessevdk/go-flags v1.5.0
	github.com/klauspost/compress v1.15.6
	github.com/stretchr/testify v1.7.5
	github.com/ulikunitz/xz v0.5.10
	github.com/vishvananda/netlink v1.1.1-0.20211118161826-650dca95af54
	github.com/xi2/xz v0.0.0-20171230120015-48954b6210f8
	github.com/yookoala/realpath v1.0.0
	golang.org/x/sys v0.0.0-20220622161953-175b2fd9d664
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/DataDog/zstd v1.4.5 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/rekby/gpt v0.0.0-20200219180433-a930afbc6edc // indirect
	github.com/sassoftware/go-rpmutils v0.2.0 // indirect
	github.com/u-root/u-root v0.8.0 // indirect
	github.com/u-root/uio v0.0.0-20210528114334-82958018845c // indirect
	github.com/vishvananda/netns v0.0.0-20210104183010-2eb08e3e575f // indirect
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad // indirect
	golang.org/x/exp v0.0.0-20220613132600-b0d781184e0d // indirect
	golang.org/x/net v0.0.0-20210405180319-a5a99cb37ef4 // indirect
	gopkg.in/freddierice/go-losetup.v1 v1.0.0-20170407175016-fc9adea44124 // indirect
)
